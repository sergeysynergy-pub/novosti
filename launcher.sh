#!/bin/bash

if [[ $1 = "--help" ]]; then
  echo "Ключи для запуска:"
  echo "init - первичная инициализация системы"
  echo "start - запустить систему со всеми контейнерами"
  echo "rebuild - пересобрать и запустить все контейнеры системы"
  echo "stop - остановить комплекс со всеми контейнерами"

  exit 0
fi

if [[ $1 = "init" ]]; then
  clear
  echo "Запускаем контейнерную группировку комплекса"
  docker-compose up -d
  echo "Создаём ссылку на каталог с модулями NodeJS"
  docker exec -d novosti_nodejs ln -sf /opt/node_modules/ node_modules

  exit 0
fi

if [[ $1 = "start" ]]; then
  clear
  # Launch containers:
  echo "Запускаем контейнерную группировку комплекса"
  docker-compose up -d
  # Wait 10 seconds to be sure all services have been launched
  sleep 5s
  # Launch starting scripts in containers
  echo "Запускаем сервисы внутри контейнеров"
  docker exec -d novosti_main ./run.sh dev
  #
  exit 0
fi

if [[ $1 = "dev" ]]; then
  clear
  # Launch containers:
  echo "Запускаем контейнерную группировку комплекса"
  docker-compose up -d
  # Launch starting scripts in containers
  echo "Запускаем сервисы внутри контейнеров"
  # docker exec -d kaokci_ccm_main ./run.sh dev

  exit 0
fi

if [[ $1 = "rebuild" ]]; then
  clear
  echo "DO NOT run this command in parallel with other docker operations!!!"
  echo "clearing docker-cache..."
  echo "--------------------------------------------------------"
  docker rmi $(docker images -a --filter=dangling=true -q)
  echo "rebuilding..."
  docker-compose up -d --build
  # Start complex
  ./launcher.sh start

  exit 0
fi

if [[ $1 = "stop" ]]; then
  # Stop containers:
  docker-compose stop

  exit 0
fi

#################################################################
### ADDITIONAL SCRIPTS
#################################################################


# DEFAULT COMMANDS WITHOUT ARGUMENTS
# ./launcher.sh --help
./launcher.sh start

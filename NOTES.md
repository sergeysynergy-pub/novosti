# Пример узлов графа

{
  "id": 455,
  "data": "ДЖУЛИЯ ПИРСОН"
  "taxonomy": {
    "tid": 10,
    "parent_tid": null,
    "name": "Персона"
  },
  "attributes": [
    {
      "id": 30,
      "name": "Фамилия",
      "value": "ПИРСОН"
    },
    {
      "id": 40,
      "name": "Отчество",
      "value": "ДЖУЛИЯ"
    },
    {
      "id": 50,
      "name": "Имя",
      "value": ""
    }
  ],
}

{
  "id": 456,
  "data": "Событие:Назначение",
  "taxonomy": {
    "tid": 60,
    "parent_tid": 50,
    "name": "Событие:Назначение"
  },
  "attributes": [
    {
      "id": 60,
      "name": "Дата",
      "value": "2014-10-03 18:47:18"
    }
  ]
}

import django
from django.urls import path, re_path
from django.conf.urls.static import static
from django.contrib import admin
#
import app.settings as settings
from zcore import views, serializers


urlpatterns = [
    path('admin/', admin.site.urls),

    # view-cтраницы, имеющие шаблоны в каталоге templates
    re_path(r'^$', views.index, name='index'),
    re_path(r'^force-d3/(?P<id>[-\w]+)/(?P<graphFilter>.*)/(?P<nodesList>.*)/(?P<color>.*)/$', views.view_force_d3, name='viewForceD3'),
    re_path(r'^graph/(?P<id>[-\w]+)/$', views.view_graph, name='viewGraph'),
    re_path(r'^force-react/(?P<id>[-\w]+)/(?P<graphFilter>.*)/$', views.view_force_react, name='viewForceReact'),
    re_path(r'^chord/(?P<id>[-\w]+)/$', views.view_chord, name='viewChord'),
    re_path(r'^timeline/(?P<id>[-\w]+)/$', views.view_timeline, name='viewTimeline'),
    re_path(r'^new-project/$', views.view_new_project, name='viewNewProject'),
    re_path(r'^map/(?P<gid>[-\w]+)/(?P<nid>[-\w]+)/$', views.view_map, name='viewMap'),

    # /view-cтраницы
    re_path(r'^create-project/(?P<graphFilter>.*)/$', views.create_project),

    # json-данные
    re_path(r'^json-heap-info/$', serializers.heap_info, name='heapInfo'),
    re_path(r'^json-force-react/(?P<id>[-\w]+)/(?P<gfilter>.*)/$', serializers.json_force_react, name='jsonForceReact'),
    #url(r'^json-force-react/(?P<id>[-\w]+)/(?P<graphFilter>.*)/$', 'zcore.views.json_force_react', name='jsonForceReact'),
    re_path(r'^json-force-d3/(?P<id>[-\w]+)/(?P<graphFilter>.*)/(?P<nodesList>.*)/(?P<color>.*)/$', serializers.json_force_d3, name='jsonForced3'),
    re_path(r'^json-chord/(?P<id>[-\w]+)/(?P<gfilter>.*)/$', serializers.json_chord, name='jsonChord'),
    re_path(r'^json-timeline/(?P<id>[-\w]+)/(?P<gfilter>.*)/$', serializers.json_timeline, name='jsonTimeline'),
    re_path(r'^json-main-graph/(?P<id>[-\w]+)/$', serializers.json_main_graph, name='jsonMainGraph'),
    re_path(r'^json-main-graph/(?P<id>[-\w]+)/(?P<gfilter>.*)/$', serializers.json_main_graph, name='jsonMainGraph'),
    re_path(r'^json-transfers/(?P<gid>[-\w]+)/(?P<nid>[-\w]+)/$', serializers.json_transfers, name='jsonTransfers'),

    # вывод справочников
    re_path(r'^json-attributes/$', serializers.json_attributes, name='jsonAttributes'),
    re_path(r'^json-taxonomy/$', serializers.json_taxonomy, name='jsonTaxonomy'),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
